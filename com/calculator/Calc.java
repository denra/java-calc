package com.calculator;

import com.lexer.Lexer;
import com.lexer.grammars.MathGrammar;
import com.lexer.support.Token;
import com.utils.ComplexNumber;
import com.utils.Vector;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Stack;


public class Calc
{
    public static ArrayList<Token> removeWhitespaces(String source)
    {
        Lexer lexer = new Lexer();
        lexer.register("math", new MathGrammar());

        ArrayList<Token> lexems = lexer.getTokens("math", source);
        ArrayList<Token> withoutWhitespaces = new ArrayList<>();
        ArrayList<Token> finalLexems = new ArrayList<>();

        for (Token lexeme : lexems)
        {
            if (!lexeme.getType().equals("whitespace"))
                withoutWhitespaces.add(lexeme);
        }

        System.out.println(withoutWhitespaces);

        for (int i = 0; i < withoutWhitespaces.size(); i++)
        {
            Token lexeme = withoutWhitespaces.get(i);

            //System.out.println(lexeme.getSubType());

            if (lexeme.getSubType().equals("sub"))
            {
                if (finalLexems.size() == 0)
                    finalLexems.add(new Token("0", "number", "real"));

                else if (withoutWhitespaces.get(i - 1).getText().equals(",") ||
                            withoutWhitespaces.get(i - 1).getText().equals("("))
                    finalLexems.add(new Token("0", "number", "real"));

            }

            finalLexems.add(lexeme);
        }

        return finalLexems;
    }

    public static ArrayList<Processable> parseParentheses(ArrayList<Token> lexems)
    {
        int dimCount = 0;

        ArrayList<Processable> finalSummands = new ArrayList<>();
        StringBuilder buffer = new StringBuilder();

        for (Token t : lexems)
        {
            if (t.getText().equals(","))
                dimCount++;
        }

        if (dimCount == 0)
        {
            for (Token t : lexems)
            {
                finalSummands.add(parseToken(t));
            }
        }
        else
        {
            ComplexNumber[] coordinates = new ComplexNumber[dimCount + 1];
            int currentCoordinate = 0;
            ArrayList<Token> lexBuffer = new ArrayList<>();

            for (Token t : lexems)
            {
                if (t.getText().equals("(") || t.getText().equals(")"))
                    continue;

                //System.out.println(t.getText());

                if (t.getText().equals(","))
                {
                    coordinates[currentCoordinate] = evaluateWithoutParentheses(lexBuffer);
                    currentCoordinate++;
                    lexBuffer.clear();
                }
                else
                    lexBuffer.add(t);
            }

            coordinates[currentCoordinate] = evaluateWithoutParentheses(lexBuffer);
            HashSet<Vector> set = new HashSet<>();
            set.add(new Vector(coordinates));

            finalSummands.add(new Operand(new ComplexNumber(0, 0), set));
        }
        return finalSummands;
    }

    public static ArrayList<Processable> parse(ArrayList<Token> lexems)
    {
        ArrayList<Processable> finalSummonds = new ArrayList<>();
        int i = 0;

        while (i < lexems.size())
        {
            Token lexeme = lexems.get(i);
            if (lexeme.getSubType().equals("left_parenthesis"))
            {
                //System.out.println("\nStart process");

                ArrayList<Token> parTokens = new ArrayList<>();
                Token currentLexeme = new Token("none", "none");
                int deep = 0;

                while (i != lexems.size())
                {
                    if ("right_parenthesis".equals(currentLexeme.getSubType()))
                    {
                        if (deep == 1)
                            break;
                        else
                            deep--;
                    }
                    if (currentLexeme.getSubType().equals("left_parenthesis"))
                        deep++;

                    currentLexeme = lexems.get(i);
                    parTokens.add(currentLexeme);

                    //System.out.println(currentLexeme);

                    i++;
                }

                System.out.println("par tokens: " + parTokens);
                System.out.println("deep: " + deep);

                for (Processable bufferSummond : parseParentheses(parTokens))
                {
                    if (bufferSummond.isOperator())
                    {
                        finalSummonds.add(bufferSummond);
                    }
                    else
                        finalSummonds.add(bufferSummond);
                }
            }

            if (i != lexems.size())
            {
                finalSummonds.add(parseToken(lexems.get(i)));

                //System.out.println(parseToken(lexems.get(i)));
            }

            i++;
        }

        return finalSummonds;
    }

    public static ComplexNumber evaluateWithoutParentheses(ArrayList<Token> lexemes)
    {
        ArrayList<Processable> summonds = new ArrayList<>();
        for (Token lexeme : lexemes)
        {
            summonds.add(parseToken(lexeme));
        }

        ArrayList<Processable> reversedSummonds = ReversePolishNotation.reverse(summonds);

        return calculateExpression(reversedSummonds).getValue();
    }

    public static Processable parseToken(Token lexeme)
    {
        if (!lexeme.getType().equals("whitespace"))
        {
            if (lexeme.getType().equals("operator"))
                return new Operator(lexeme.getSubType());

            if (lexeme.getType().equals("number"))
            {
                if (lexeme.getSubType().equals("real"))
                    return new Operand(new ComplexNumber(Integer.parseInt(lexeme.getText()), 0), new HashSet<>());

                if (lexeme.getSubType().equals("special"))
                    return new Operand(new ComplexNumber(0, Integer.parseInt(lexeme.getText())), new HashSet<>());
            }
        }

        throw new IllegalArgumentException();
    }

    public static Operand calculateExpression(ArrayList<Processable> reversedSummonds)
    {
        Stack<Processable> processStack = new Stack<>();

        for (Processable summond : reversedSummonds)
        {
            if (!summond.isOperator())
            {
                processStack.push(summond);
            }
            else
            {
                Operator operator = (Operator) summond;
                Operand firstOperand = (Operand) processStack.pop();
                Operand secondOperand = (Operand) processStack.pop();

                if (operator.type.equals("add"))
                    processStack.push(Operand.sum(firstOperand, secondOperand));

                else if (operator.type.equals("multiply"))
                    processStack.push(Operand.multiply(firstOperand, secondOperand));

                else if (operator.type.equals("sub"))
                    processStack.push(Operand.subtract(secondOperand, firstOperand));

                else if (operator.type.equals("int_div"))
                    processStack.push(Operand.intDiv(secondOperand, firstOperand));
            }
        }

        return (Operand) processStack.pop();
    }

    public static String processSource(String source) {
        return source + " =";
    }

    public static Operand calculate(String source)
    {
        //System.out.println(String.format("Source: {%s}", source));
        ArrayList<Token> lexemes = removeWhitespaces(processSource(source));

        //System.out.println(String.format("Lexems: %s", lexems));
        ArrayList<Processable> summonds = parse(lexemes);

        //System.out.println(String.format("Summonds: %s", summonds));
        ArrayList<Processable> reversedSummonds = ReversePolishNotation.reverse(summonds);

        //System.out.println(String.format("Reversed summonds: %s", reversedSummonds));
        Operand result = calculateExpression(reversedSummonds);

        //System.out.println(String.format("Result: %s", result));
        return result;
    }
}