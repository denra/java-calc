package com.calculator;


public class Operator implements Processable {
    public String type;

    public Operator(String type)
    {
        this.type = type;
    }

    public int getPriority()
    {
        if (type.equals("left_parenthesis"))
            return 0;
        if (type.equals("right_parenthesis"))
            return 1;
        if (type.equals("add"))
            return 2;
        if (type.equals("sub"))
            return 3;
        if (type.equals("multiply"))
            return 4;
        if (type.equals("int_div"))
            return 4;
        return 5;
    }

    @Override
    public boolean isOperator() {
        return true;
    }

    @Override
    public String toString() {
        return type;
    }
}