package com.calculator;


public interface Processable {
    public boolean isOperator();
}