package com.calculator;

import java.util.ArrayList;
import java.util.Stack;


public class ReversePolishNotation
{
    public static ArrayList<Processable> reverse(ArrayList<Processable> summonds)
    {
        ArrayList<Processable> outputExpression = new ArrayList<>();
        Stack<Operator> operStack = new Stack<>();

        for (Processable summond : summonds)
        {
            if (!summond.isOperator())
                outputExpression.add(summond);
            else
            {
                Operator summonOperator = ((Operator) summond);

                if (summonOperator.type.equals("left_parenthesis"))
                    operStack.push(summonOperator);

                else if (summonOperator.type.equals("right_parenthesis"))
                {
                    Operator operator = operStack.pop();

                    while (!operator.type.equals("left_parenthesis"))
                    {
                        outputExpression.add(operator);
                        operator = operStack.pop();
                    }
                }
                else
                {
                    if (operStack.size() > 0)
                        if (summonOperator.getPriority() <= operStack.peek().getPriority())
                            outputExpression.add(operStack.pop());

                    operStack.push(summonOperator);
                }
            }
        }

        while (operStack.size() > 0)
            outputExpression.add(operStack.pop());

        return outputExpression;
    }
}