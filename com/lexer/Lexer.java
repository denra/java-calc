package com.lexer;

import com.lexer.grammars.Grammar;
import com.lexer.parsers.Parser;
import com.lexer.support.Token;

import java.util.ArrayList;
import java.util.HashMap;


public class Lexer
{
    private HashMap<String, Grammar> langMap = new HashMap<>();

    public void register(String language, Grammar grammar)
    {
        langMap.put(language, grammar);
    }
    
    public ArrayList<Token> getTokens(String language, String programm)
    {
        Grammar grammar = langMap.get(language);
        Parser parser = grammar.getParser();
        return parser.parse(programm, grammar.getParserSupport());
    }
    
    public String translate(String fromLanguage, String toLanguage, String sourceCode)
    {
        Grammar fromGrammar = langMap.get(fromLanguage);
        Grammar toGrammar = langMap.get(toLanguage);

        StringBuilder resultBuffer = new StringBuilder();

        // Разбиваем исходный код на токены
        ArrayList<Token> tokens = fromGrammar.getParser().parse(sourceCode, fromGrammar.getParserSupport());

        // Обрабатываем все токены
        Token curToken;
        Token prevToken = null;
        for (int i = 0; i < tokens.size(); i++)
        {
            curToken = tokens.get(i);
            String type = curToken.getType();
            String subtype = curToken.getSubType();

            if (type.equals("other") || type.equals("whitespace") ||
                    type.equals("number"))
                resultBuffer.append(curToken.getText());
            else
            {
                // Если мы переводим прогу с паскаля,
                // то после завершения логического блока ";" быть не должно.
                if (i > 0 && prevToken.getText().equals("end") &&
                        curToken.getText().equals(";"))
                    continue;

                // Проходим по всем зарегистрированным токенам языка,
                // в который мы переводим исходную программу.
                for (Token finalGrammarToken : toGrammar.getParser().getTokens())
                {
                    // Если мы нашли токен с соответствующим типом
                    // в языке toLanguage,
                    // то транслируем исходную инструкцию.
                    if (finalGrammarToken.getSubType().equals(subtype))
                    {
                        resultBuffer.append(finalGrammarToken.getText());
                        break;
                    }
                }
            }

            prevToken = curToken;
        }

        return resultBuffer.toString();
    }
}
