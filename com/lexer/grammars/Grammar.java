package com.lexer.grammars;

import com.lexer.parsers.Parser;
import com.lexer.support.ParserSupport;


public class Grammar
{
    protected ParserSupport parserSupport;
    private Parser parser;

    public Grammar(Parser parser, ParserSupport parserSupport)
    {
        this.parser = parser;
        this.parserSupport = parserSupport;
    }

    public ParserSupport getParserSupport() {
        return parserSupport;
    }
    public Parser getParser() {
        return parser;
    }
}
