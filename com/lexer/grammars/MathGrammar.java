package com.lexer.grammars;

import com.lexer.parsers.MathParser;
import com.lexer.support.ParserSupport;


public class MathGrammar extends Grammar {
    public MathGrammar() {
        super(new MathParser(), null);
        ParserSupport ParserSupport = new ParserSupport("//", "/*", "*/",
                                                        false, 'i');
        this.parserSupport = ParserSupport;
    }
}