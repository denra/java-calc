package com.lexer.parsers;

import com.lexer.support.Token;


public class MathParser extends Parser {
    public MathParser() {
        super();
        addToken(new Token("+", "operator", "add"));
        addToken(new Token("-", "operator", "sub"));
        addToken(new Token("*", "operator", "multiply"));
        addToken(new Token("/", "operator", "int_div"));
        addToken(new Token("(", "operator", "left_parenthesis"));
        addToken(new Token(")", "operator", "right_parenthesis"));
        addToken(new Token(",", "operator", "comma"));
        addToken(new Token("i", "operator", "complex"));
    }
}