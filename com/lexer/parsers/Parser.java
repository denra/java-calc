package com.lexer.parsers;

import com.lexer.support.CharType;
import com.lexer.support.ParserSupport;
import com.lexer.support.Token;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;


public class Parser {
    private ArrayList<Token> tokens;

    public Parser()
    {
        tokens = new ArrayList<>();
    }

    public void addToken(Token newToken)
    {
        tokens.add(newToken);
    }

    private static CharType getCharType(char ch)
    {
        if (Character.isWhitespace(ch))
            return CharType.WHITESPACE;

        if (Character.isAlphabetic(ch) || Character.isDigit(ch))
            return CharType.ALPHABETIC;

        return CharType.OTHER;
    }

    private static Comparator<Token> compByLen()
    {
        // Сначала элементы с большей длиной, затем с меньшей.
        return new Comparator<Token>() {
            @Override
            public int compare(Token s1, Token s2) {
                return new Integer(s2.getText().length()).compareTo(s1.getText().length());
            }
        };
    }

    public ArrayList<Token> getTokens()
    {
        return (ArrayList<Token>)tokens.clone();
    }

    public ArrayList<Token> getTokens(String type, int len)
    {
        ArrayList<Token> newKeyWords = new ArrayList<>();

        Token keyWord;
        int keyWordsSize = tokens.size();

        // Берем все токены, длина которных НЕ превышает len и
        // тип которых равен type.
        for (int i = 0; i < keyWordsSize; i++)
        {
            keyWord = tokens.get(i);

            if (keyWord.getText().length() <= len &&
                    keyWord.getType() == type)
                newKeyWords.add(keyWord);
        }

        // Сортируем токены по убыванию длины.
        Collections.sort(newKeyWords, compByLen());

        return newKeyWords;
    }

    private ArrayList<Token> parseOperators(String operators)
    {
        StringBuilder buffer = new StringBuilder(operators);
        ArrayList<Token> foundOps = new ArrayList<>();
        boolean found;

        // Берем все операторы, отсортированные по убыванию длины.
        ArrayList<Token> ops = getTokens("operator", operators.length());
        int opsSize = ops.size();
        Token operator = null;

        // Обрабатываем переданные операторы
        while (buffer.length() != 0)
        {
            found = false;

            // Ищем первый подходящий оператор,
            // с которого начинается строка buffer.toString()
            for (int i = 0; i < opsSize; i++)
            {
                operator = ops.get(i);
                if (buffer.toString().startsWith(operator.getText()))
                {
                    foundOps.add(operator);
                    buffer = new StringBuilder(buffer.substring(operator.getText().length()));
                    found = true;
                    break;
                }
            }

            // Если больше не было найдено других операторов в начале buffer,
            // оставшийся "хвост" добавляем как не распознанный оператор и
            // выходим из цикла обработки.
            if (!found)
            {
                foundOps.add(new Token(buffer.toString(), "unparsable_operator"));
                break;
            }
        }

        return foundOps;
    }

    private Token getToken(String text, Pattern numPattern,
                           Pattern specNumPattern)
    {
        // Если входной text - это зарегистрированный токен,
        // то возвращаем этот токен.
        for (Token t : tokens)
        {
            if (t.getText().equals(text))
                return t;
        }

        // Иначе если это число, то возвращаем токен типа number
        Matcher specNumMatcher = specNumPattern.matcher(text);
        Matcher numMatcher = numPattern.matcher(text);

        if (specNumMatcher.matches())
            return new Token(text, "number", "special");
        if (numMatcher.matches())
            return new Token(text, "number", "real");

        // Иначе это токен типа other.
        return new Token(text, "other");
    }

    private static String fixCommentSymbols(String commentSymbols)
    {
        // Если символs комментария нельзя скомпилировать в паттерн,
        // значит некоторые из них являются служебными для рег. выр-ов.
        try
        {
            Pattern.compile(commentSymbols);
        }
        catch (PatternSyntaxException e)
        {
            // Поэтому такие символы нужно обезоружить обратным слешем.
            StringBuilder buffer = new StringBuilder();

            String symb;
            for (int i = 0; i < commentSymbols.length(); i++)
            {
                symb = commentSymbols.substring(i, i + 1);

                try
                {
                    Pattern.compile(symb);
                    buffer.append(symb);
                }
                catch (PatternSyntaxException e1)
                {
                    buffer.append("\\");
                    buffer.append(symb);
                }
            }

            commentSymbols = buffer.toString();
        }

        return commentSymbols;
    }

    private static String removeBlockComments(String source, String startSymb, String endSymb)
    {
        String replacingBlock = fixCommentSymbols(startSymb) + "[\\d\\D]*" + fixCommentSymbols(endSymb);
        return source.replaceAll(replacingBlock, "");
    }

    private static String removeLineComments(String source, String commentSymbol)
    {
        return source.replaceAll(commentSymbol + ".*", "");
    }

    private void parseToken(ArrayList<Token> textTokens, String tokenText,
                            char curTokenStartChar, Pattern numPattern,
                            Pattern specNumPattern)
    {
        if (getCharType(curTokenStartChar) == CharType.OTHER)
        {
            ArrayList<Token> opTokens = parseOperators(tokenText);

            for (int j = 0; j < opTokens.size(); j++)
                textTokens.add(opTokens.get(j));
        }
        else
            if (getCharType(curTokenStartChar) == CharType.WHITESPACE)
                textTokens.add(new Token(tokenText, "whitespace"));
            else
                textTokens.add(getToken(tokenText, numPattern, specNumPattern));
    }

    private ArrayList<Token> parseTokens(String text, ParserSupport parserSupport)
    {
        ArrayList<Token> textTokens = new ArrayList<>();

        Character endNumSymbol = parserSupport.getEndNumberSymbol();
        Pattern specNumPattern = Pattern.compile("\\d+" + endNumSymbol);
        Pattern numPattern = Pattern.compile("\\d+");

        char curTokenStartChar = text.charAt(0);
        int curTokenStartPosition = 0;
        String tokenText;

        for (int i = 0; i < text.length(); i++)
        {
            if (getCharType(text.charAt(i)) != getCharType(curTokenStartChar))
            {
                tokenText = text.substring(curTokenStartPosition, i);

                // Обработываем прочитанный токен.
                parseToken(textTokens, tokenText, curTokenStartChar,
                           numPattern, specNumPattern);

                curTokenStartChar = text.charAt(i);
                curTokenStartPosition = i;
            }
        }

        // Обработываем последний прочитанный токен.
        tokenText = text.substring(curTokenStartPosition, text.length());
        parseToken(textTokens, tokenText, curTokenStartChar,
                   numPattern, specNumPattern);

        return textTokens;
    }

    public ArrayList<Token> parse(String text, ParserSupport parserSupport)
    {
        // Если парсер регистроНЕзависим,
        // все символы text делаем строчными.
        if (!parserSupport.getCaseSensitive())
            text = text.toLowerCase();

        // Избавляем text от комментариев.
        text = removeBlockComments(text, parserSupport.getStartBlockSymbol(), parserSupport.getEndBlockSymbol());
        text = removeLineComments(text, parserSupport.getLineCommentSymbol());

        // Находим все токены в text.
        return parseTokens(text, parserSupport);
    }
}