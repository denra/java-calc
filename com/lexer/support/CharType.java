package com.lexer.support;

public enum CharType {
    WHITESPACE, ALPHABETIC, OTHER;
}
