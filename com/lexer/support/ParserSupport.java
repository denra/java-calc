package com.lexer.support;


public class ParserSupport {
    private String lineCommentSymbol;
    private String startBlockSymbol;
    private String endBlockSymbol;
    private boolean caseSensitive;
    private Character endNumberSymbol;

    public ParserSupport(String lineCommentSymbol, String startBlockSymbol,
                         String endBlockSymbol, boolean caseSensitive,
                         Character endNumberSymbol)
    {
        this.lineCommentSymbol = lineCommentSymbol;
        this.startBlockSymbol = startBlockSymbol;
        this.endBlockSymbol = endBlockSymbol;
        this.caseSensitive = caseSensitive;
        this.endNumberSymbol = endNumberSymbol;
    }

    public Character getEndNumberSymbol()
    {
        return endNumberSymbol;
    }

    public boolean getCaseSensitive() {
        return caseSensitive;
    }

    public String getLineCommentSymbol() {
        return lineCommentSymbol;
    }

    public String getStartBlockSymbol() {
        return startBlockSymbol;
    }

    public String getEndBlockSymbol() {
        return endBlockSymbol;
    }
}
