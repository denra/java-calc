package com.utils;

public class ComplexNumber {
    private int real;
    private int imagine;

    public ComplexNumber(int real, int imagine)
    {
        this.real = real;
        this.imagine = imagine;
    }

    public int getReal() {
        return real;
    }

    public int getImagine() {
        return imagine;
    }

    public ComplexNumber add(ComplexNumber number)
    {
        int newReal = real + number.getReal();
        int newImagine = imagine + number.getImagine();

        return new ComplexNumber(newReal, newImagine);
    }

    public ComplexNumber sub(ComplexNumber number)
    {
        int newReal = real - number.getReal();
        int newImagine = real - number.getImagine();

        return new ComplexNumber(newReal, newImagine);
    }

    public ComplexNumber mul(ComplexNumber number)
    {
        int a1 = real;
        int b1 = imagine;

        int a2 = number.getReal();
        int b2 = number.getImagine();

        int newReal = a1 * a2 - b1 * b2;
        int newImagine = a1 * b2 + b1 * a2;

        return new ComplexNumber(newReal, newImagine);
    }

    public ComplexNumber div(ComplexNumber number)
    {
        int a1 = real;
        int b1 = imagine;

        int a2 = number.getReal();
        int b2 = number.getImagine();

        int den = a2 * a2 + b2 * b2;

        int newReal = (a1 * a2 + b1 * b2) / den;
        int newImagine = (a2 * b1 - a1 * b2) /den;

        return new ComplexNumber(newReal, newImagine);
    }

    public static ComplexNumber parseComplexNumber(String source)
    {
        return new ComplexNumber(Integer.parseInt(source), 0);
    }

    @Override
    public int hashCode() {
        return real + imagine;
    }

    @Override
    public boolean equals(Object object)
    {
        if (!(object instanceof ComplexNumber))
            return false;
        if (hashCode() != object.hashCode())
            return false;
        if (imagine == ((ComplexNumber) object).getImagine() &&
                real == ((ComplexNumber) object).getReal())
            return true;

        return false;
    }

    @Override
    public String toString() {
        return real + " + " + imagine + "i";
    }
}