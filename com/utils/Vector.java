package com.utils;


public class Vector {
    private ComplexNumber[] coordinates;

    public Vector(ComplexNumber[] coordinates)
    {
        for (int i = 0; i < coordinates.length; i++) {
            this.coordinates = coordinates;
        }
    }

    public ComplexNumber[] getCoordinates()
    {
        return coordinates;
    }

    public int getCount() {
        return coordinates.length;
    }

    @Override
    public int hashCode()
    {
        ComplexNumber sum = new ComplexNumber(0,0);

        for (ComplexNumber coord : coordinates)
        {
            sum = sum.add(coord);
        }

        return sum.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof Vector))
            return false;
        if (this.getCount() != ((Vector) obj).getCount())
            return false;
        if (this.hashCode() != obj.hashCode())
            return false;

        ComplexNumber[] objectCoords = ((Vector) obj).getCoordinates();
        for (int i = 0; i < coordinates.length; i++) {
            if (!objectCoords[i].equals(coordinates[i]))
                return false;
        }
        return true;
    }

    public Vector add(Vector vector)
    {
        ComplexNumber[] addingCoordinates = vector.getCoordinates();
        ComplexNumber[] newCoordinates = new ComplexNumber[addingCoordinates.length];

        for (int i = 0; i < coordinates.length; i++)
        {
            newCoordinates[i] = coordinates[i].add(addingCoordinates[i]);
        }
        return new Vector(newCoordinates);
    }

    public Vector subtract(Vector vector) {
        ComplexNumber[] subtractingCoordinates = vector.getCoordinates();
        ComplexNumber[] newCoordinates = new ComplexNumber[subtractingCoordinates.length];

        for (int i = 0; i < coordinates.length; i++)
        {
            newCoordinates[i] = coordinates[i].sub(subtractingCoordinates[i]);
        }
        return new Vector(newCoordinates);
    }

    public Vector intDiv(ComplexNumber number) {
        if (number.equals(new ComplexNumber(0, 0)))
        {
            throw new IllegalArgumentException("Division by zero");
        }

        ComplexNumber[] newCoordinates = new ComplexNumber[coordinates.length];
        for (int i = 0; i < coordinates.length; i++)
        {
            newCoordinates[i] = coordinates[i].div(number);
        }
        return new Vector(newCoordinates);
    }

    public boolean isZeroVector()
    {
        for (ComplexNumber x : coordinates)
        {
            if (!x.equals(new ComplexNumber(0, 0)))
                return false;
        }
        return true;
    }

    public ComplexNumber scalarProduct(Vector vector)
    {
        if (getCount() != vector.getCount())
            throw new IllegalArgumentException();

        ComplexNumber sum = new ComplexNumber(0, 0);
        for (int i = 0; i < getCount(); i++)
        {
            sum = sum.add(coordinates[i].mul(vector.getCoordinates()[i]));
        }
        return sum;
    }

    public Vector multiply(ComplexNumber number)
    {
        ComplexNumber[] newCoordinates = new ComplexNumber[coordinates.length];

        for (int i = 0; i < coordinates.length; i++)
        {
            newCoordinates[i] = coordinates[i].mul(number);
        }
        return new Vector(newCoordinates);
    }

    @Override
    public String toString()
    {
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < coordinates.length - 1; i++)
        {
            buffer.append(coordinates[i].toString());
            buffer.append(", ");
        }

        buffer.append(coordinates[coordinates.length - 1]);
        return String.format("Vector(%s)", buffer.toString());
    }
}
